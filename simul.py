# Autor: Ravi Helon de Melo Serafim Ferreira

# Descrição:
"""
    Esta função realiza a simulação para determinada razão sinal ruído e número de símbolos
"""
from numpy import linspace
from MI import calc_mi
from matplotlib import pyplot as plt


def sim(m, snr):
    """
    Implementação da simulação

    :param m: Quantidade de símbolo
    :param snr: Razão sinal-ruído
    :return: Informação Mútua
    """

    MI = []

    for n in snr:
        nw = 10 ** (n / 10)
        mi = calc_mi(m, nw)
        MI.append(mi)

    plt.plot(MI)
    plt.show()


if __name__ == "__main__":

    b = 4
    a = linspace(1, 20, 20)

    sim(b, a)
