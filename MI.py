# Autor: Ravi Helon de Melo Serafim Ferreira

# Descrição:
"""
    Esta função foi implementado com o objetivo de calcular a informação mútua entre duas variáveis aleatórias
onde uma deles obedece uma constelação m-qam e a outra é o resultado da soma da primeira com ruído branco adi-
tivo. A equação utilizada para tal é a descrita por:

$I = \frac{1}{M} \sum_i\int_v \frac{1}{\sqrt{2\pi}} e^{-\frac{v^2}{2}}( \log M - \log e^{-\frac{(x_i-x_j)v}
{2\sigma}-\frac{(x_i-x_j)^2}{2\sigma^2}} )dv$

    A integral está implementada pelo método do trapézio
"""
from numpy import log2, sqrt, linspace, pi
from Simbols import coord
from Integral.Trapezio import inte
from scipy.integrate import dblquad

def calc_mi(m, snr):
    """
    Calcula a Informação Mútua

    :param m: Quantidade de Símbolos da constelação
    :param snr: Razão sinal-ruído (SNR - Signal-to-Noise Ratio)
    :return: Informação Mútua calculada
    """
    x = coord(m)

    outer_sum = 0

    S = sum(xi**2 for xi in x)/m
    std = sqrt(S/snr)

    x0 = min(x)-6*std
    xn = -x0

    # Quantidade de iterações para o método do trapézio
    D = 1000
    valores, h = linspace(x0, xn, D, retstep=True)

    for j in range(0, m - 1):
        for i in range(0, m - 1):
            integral, err = dblquad(inte,x0,xn,x0,xn, args = (x[i],x[j],x,std))
            outer_sum = outer_sum + float(integral)
            print(integral)
    MI = log2(m*m) - 1/m*1/m * 1 / sqrt(2 * pi) * outer_sum/60

    return MI
