# Autor: Ravi Helon de Melo Serafim Ferreira

# Descrição:
"""
    Esta função serve para calcular as amplitudes dos símbolos presentes na constelação
MQAM de acordo com a expressão:

    X = {-\sqrt(M)-1, ..., -3, -1, 1, 3, ..., \sqrt(M)-1}
"""


def coord(m):
    """
    Calcula Amplitude dos sinais da constelação

    :param m: Quantidade de símbolos da constelação
    :return: Vetor com os símbolos
    """
    x = []
    try:
        for n in range(m):
            if n % 2 != 0:
                x.append(n)
                x.append((-1) * n)
        x.sort()
        return x
    except ValueError:
        print("Não existe {}QAM".format(m))
