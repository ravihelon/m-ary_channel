"""
    Este Módulo objetiva a realização de uma simulação para cálculo da informação mútua a partir da razão
sinal ruído e da quantidade de símbolos
"""
from MI import calc_mi
from Simbols import coord
from simul import sim
import Integral
