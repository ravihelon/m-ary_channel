# Autor: Ravi Helon de Melo Serafim Ferreira

# Descrição:
"""
    Esta função implementa o método do trapézio para a integral da informação mútua para canal com ruído
aditivo gaussiano branco, de acordo com a equação a seguir:

 $\int_a^b f(x) dx \approx \frac{h}{2} [ f(x_0) + f(x_n) + 2 * \sum_{i=1}^{n-1} f(x_i) ]$
"""
from numpy import log2, exp, abs


def inte(x,y,x_i,x_j,c,std):
    """
    Implementação do método do trapézio.

    :param x: Vetor das variáveis de entrada
    :param x_0: Valor da normalização de y
    :param std: Valor do desvio padrão do ruído
    :return: Retorna o valor final da integral
    """
    v = (x - x_i) / std
    lgsum = 0
    w = (y - x_j) / std
    lgsum = 0
    for j in range(0, len(c) - 1):
        v_i = (x_i - c[j])
        w_i = (x_j - c[j])
        exponente1 = - abs(v_i**2-w_i**2) / std**2 / 2
        exponente2 = (v*v_i + w*w_i)/ 2 / std
        lgsum = lgsum + exp(exponente1 + exponente2)
    b = log2(lgsum) * exp(-abs(v**2 -w**2)/ 2)

    return b
