# Autor: Ravi Helon de Melo Serafim Ferreira

# Descrição:
"""
    Este módulo compila diferentes forma de realização de integral numérica para o problema de determinação
da informação mútua.
    Atualmente apenas o método do trapézio, na verdade =x
"""
from .Trapezio import inte
